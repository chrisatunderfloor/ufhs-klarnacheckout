# note: this should never truly be refernced since we are using relative assets
http_path = "/skin/frontend/base/default/"
css_dir = "../"
sass_dir = "../scss"
images_dir = "../images"
generated_images_dir = "../images/generated"
javascripts_dir = "../js"

relative_assets = true

environment = :development
output_style = (environment == :development) ? :expanded : :compressed
line_comments = (environment == :development) ? true : false

sass_options = {:sourcemap => true}

# compass watch -e development