<?php
/**
 * Copyright 2018 Klarna Bank AB (publ)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Kco
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Generate order lines for order items
 */
class Klarna_Kco_Model_Checkout_Orderline_Items extends Klarna_Kco_Model_Checkout_Orderline_Abstract
{
    /**
     * Checkout item types
     */
    const ITEM_TYPE_PHYSICAL = 'physical';
    const ITEM_TYPE_VIRTUAL  = 'digital';

    /**
     * Tax calculation model
     *
     * @var Mage_Tax_Model_Calculation
     */
    protected $_calculator;
    /**
     * Order lines is not a total collector, it's a line item collector
     *
     * @var bool
     */
    protected $_isTotalCollector = false;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->_calculator = Mage::getSingleton('tax/calculation');
    }

    /**
     * Collect totals process.
     *
     * @param Klarna_Kco_Model_Api_Builder_Abstract $checkout
     *
     * @return $this
     */
    public function collect($checkout)
    {
        $object = $checkout->getObject();
        $helper = Mage::helper('klarna_kco/checkout');
        $items  = array();

        foreach ($object->getAllItems() as $item) {
            $qtyMultiplier = 1;

            // Order item checks
            if (($item instanceof Mage_Sales_Model_Order_Invoice_Item
                || $item instanceof Mage_Sales_Model_Order_Creditmemo_Item)
            ) {
                $orderItem  = $item->getOrderItem();
                $parentItem = $orderItem->getParentItem()
                    ?: ($orderItem->getParentItemId() ? $object->getItemById($orderItem->getParentItemId()) : null);

                // Skip if child product of a non bundle parent
                if ($parentItem && Mage_Catalog_Model_Product_Type::TYPE_BUNDLE != $parentItem->getProductType()) {
                    continue;
                }

                // Skip if a bundled product with price type dynamic
                if ((Mage_Catalog_Model_Product_Type::TYPE_BUNDLE == $orderItem->getProductType()
                    && Mage_Bundle_Model_Product_Price::PRICE_TYPE_DYNAMIC == $orderItem->getProduct()->getPriceType())
                ) {
                    continue;
                }

                // Skip if child product of a bundle parent and bundle product price type is fixed
                if ($parentItem && Mage_Catalog_Model_Product_Type::TYPE_BUNDLE == $parentItem->getProductType()
                    && Mage_Bundle_Model_Product_Price::PRICE_TYPE_FIXED == $parentItem->getProduct()->getPriceType()
                ) {
                    continue;
                }

                // Skip if parent is a bundle product having price type dynamic
                if ($parentItem && Mage_Catalog_Model_Product_Type::TYPE_BUNDLE == $orderItem->getProductType()
                    && Mage_Bundle_Model_Product_Price::PRICE_TYPE_DYNAMIC == $orderItem->getProduct()->getPriceType()
                ) {
                    continue;
                }
            }

            // Quote item checks
            if ($item instanceof Mage_Sales_Model_Quote_Item) {
                // Skip if bundle product with a dynamic price type
                if (Mage_Catalog_Model_Product_Type::TYPE_BUNDLE == $item->getProductType()
                    && Mage_Bundle_Model_Product_Price::PRICE_TYPE_DYNAMIC == $item->getProduct()->getPriceType()
                ) {
                    continue;
                }

                // Get quantity multiplier for bundle products
                if ($item->getParentItemId() && ($parentItem = $object->getItemById($item->getParentItemId()))) {
                    // Skip if non bundle product or if bundled product with a fixed price type
                    if (Mage_Catalog_Model_Product_Type::TYPE_BUNDLE != $parentItem->getProductType()
                        || Mage_Bundle_Model_Product_Price::PRICE_TYPE_FIXED == $parentItem->getProduct()->getPriceType()
                    ) {
                        continue;
                    }

                    $qtyMultiplier = $parentItem->getQty();
                }
            }

            $_item = array(
                'type'          => $item->getIsVirtual() ? self::ITEM_TYPE_VIRTUAL : self::ITEM_TYPE_PHYSICAL,
                'reference'     => substr($item->getSku(), 0, 64),
                'name'          => $item->getName(),
                'quantity'      => ceil($item->getQty() * $qtyMultiplier),
                'discount_rate' => 0
            );

            if ($helper->getSeparateTaxLine($object->getStore())) {
                $_item['tax_rate']         = 0;
                $_item['total_tax_amount'] = 0;
                $_item['unit_price']       = $helper->toApiFloat($item->getBasePrice())
                    ?: $helper->toApiFloat($item->getBaseOriginalPrice());
                $_item['total_amount']     = $helper->toApiFloat($item->getBaseRowTotal());
            } else {
                $taxRate = 0;
                if ($item->getBaseRowTotal() > 0) {
                    $taxRate = ($item->getTaxPercent() > 0) ? $item->getTaxPercent()
                        : ($item->getBaseTaxAmount() / $item->getBaseRowTotal() * 100);
                }

                $totalTaxAmount = $this->_calculator->calcTaxAmount($item->getBaseRowTotalInclTax(), $taxRate, true);

                $_item['tax_rate']         = $helper->toApiFloat($taxRate);
                $_item['total_tax_amount'] = $helper->toApiFloat($totalTaxAmount);
                $_item['unit_price']       = $helper->toApiFloat($item->getBasePriceInclTax());
                $_item['total_amount']     = $helper->toApiFloat($item->getBaseRowTotalInclTax());
            }

            $_item = new Varien_Object($_item);
            Mage::dispatchEvent(
                'kco_orderline_item', array(
                'checkout'    => $checkout,
                'object_item' => $item,
                'klarna_item' => $_item
                )
            );

            $items[] = $_item->toArray();

            $checkout->setItems($items);
        }

        return $this;
    }

    /**
     * Add order details to checkout request
     *
     * @param Klarna_Kco_Model_Api_Builder_Abstract $checkout
     *
     * @return $this
     */
    public function fetch($checkout)
    {
        if ($checkout->getItems()) {
            foreach ($checkout->getItems() as $item) {
                $checkout->addOrderLine($item);
            }
        }

        return $this;
    }
}
