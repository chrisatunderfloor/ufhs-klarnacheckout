<?php
/**
 * Copyright 2018 Klarna Bank AB (publ)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Kco
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * API callback controller for Klarna
 */
class Klarna_Kco_ApiController extends Klarna_Kco_Controller_Klarna
{
    /**
     * API call to update address details on a customers quote via callback from Klarna
     */
    public function addressUpdateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $klarnaOrderId = $this->getRequest()->get('id');
            $quote         = Mage::helper('klarna_kco/checkout')->loadQuoteByCheckoutId($klarnaOrderId);

            if (!$quote->getId()) {
                $this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->setHttpResponseCode(301);
                $failUrl = Mage::getUrl(
                    'checkout/klarna/validateFailed', array(
                    '_nosid'  => true,
                    '_escape' => false
                    )
                );
                $this->getResponse()->setHeader('Location', $failUrl);

                return;
            }

            $this->getKco()->setQuote($quote);

            $body = $this->getRequest()->getRawBody();

            try {
                $checkout = Mage::helper('core')->jsonDecode($body);
                $checkout = new Varien_Object($checkout);
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_sendBadRequestResponse();

                return;
            }

            $this->_updateOrderAddresses($checkout);

            try {
                $response = $this->getKco()->getApiInstance()->getGeneratedUpdateRequest();
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_sendBadRequestResponse('Unknown error');

                return;
            }

            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        } catch (Klarna_Kco_Exception $e) {
            // Do not modify header or log exception
            Mage::logException($e);
        } catch (Klarna_Kco_Model_Api_Exception $e) {
            Mage::logException($e);
            $this->getResponse()->setHeader('HTTP/1.1', '503 Service Unavailable')->sendResponse();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    /**
     *  API call to set shipping method on a customers quote via callback from Klarna
     */
    public function shippingMethodUpdateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $klarnaOrderId = $this->getRequest()->get('id');
            $quote         = Mage::helper('klarna_kco/checkout')->loadQuoteByCheckoutId($klarnaOrderId);

            if (!$quote->getId()) {
                $this->getResponse()->setHeader('Content-type', 'application/json');
                $this->getResponse()->setHttpResponseCode(404);
                $this->getResponse()->setBody(
                    Mage::helper('core')->jsonEncode(
                        array(
                        'error' => 'Order not found'
                        )
                    )
                );

                return;
            }

            $this->getKco()->setQuote($quote);

            $body = $this->getRequest()->getRawBody();

            try {
                $checkout = Mage::helper('core')->jsonDecode($body);
                $checkout = new Varien_Object($checkout);
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_sendBadRequestResponse();

                return;
            }

            if (($selectedOption = $checkout->getSelectedShippingOption()) && isset($selectedOption['id'])) {
                try {
                    $this->getKco()->saveShippingMethod($selectedOption['id']);
                    $this->getKco()->getQuote()->collectTotals()->save();
                } catch (Exception $e) {
                    $this->getKco()->checkShippingMethod();
                    $this->getKco()->getQuote()->collectTotals()->save();
                }
            }

            try {
                $response = $this->getKco()->getApiInstance()->getGeneratedUpdateRequest();
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_sendBadRequestResponse('Unknown error');

                return;
            }

            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        } catch (Klarna_Kco_Model_Api_Exception $e) {
            Mage::logException($e);
            $this->getResponse()->setHeader('HTTP/1.1', '503 Service Unavailable')->sendResponse();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    /**
     * API call to notify Magento that the order is now ready to receive order management calls
     */
    public function pushAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $helper             = Mage::helper('klarna_kco');
        $checkoutHelper     = Mage::helper('klarna_kco/checkout');
        $checkoutId         = $this->getRequest()->getParam('id');
        $responseCodeObject = new Varien_Object(
            array(
            'response_code' => 200
            )
        );

        try {
            $klarnaOrder = Mage::getModel('klarna_kco/klarnaorder')->loadByCheckoutId($checkoutId);
            $order       = Mage::getModel('sales/order')->load($klarnaOrder->getOrderId());

            if (!$order->getId()) {
                throw new Klarna_Kco_Exception('Order not found');
            }

            $store = $order->getStore();

            Mage::dispatchEvent(
                'kco_push_notification_before', array(
                'order'                => $order,
                'klarna_order_id'      => $checkoutId,
                'response_code_object' => $responseCodeObject,
                )
            );

            $api = $helper->getApiInstance($store);

            // Add comment to order and update status if still in payment review
            if ($order->getState() == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
                /** @var Mage_Sales_Model_Order_Payment $payment */
                $payment = $order->getPayment();
                $payment->registerPaymentReviewAction(Mage_Sales_Model_Order_Payment::REVIEW_ACTION_UPDATE, true);

                $statusObject = new Varien_Object(
                    array(
                    'status' => $checkoutHelper->getProcessedOrderStatus($order->getStore())
                    )
                );

                Mage::dispatchEvent(
                    'kco_push_notification_before_set_state', array(
                    'order'         => $order,
                    'klarna_order'  => $klarnaOrder,
                    'status_object' => $statusObject
                    )
                );

                if (Mage_Sales_Model_Order::STATE_PROCESSING == $order->getState()) {
                    $order->addStatusHistoryComment(
                        $helper->__('Order processed by Klarna.'),
                        $statusObject->getStatus()
                    );
                }
            }

            $checkoutType = $checkoutHelper->getCheckoutType($store);
            Mage::dispatchEvent(
                "kco_push_notification_after_type_{$checkoutType}", array(
                'order'                => $order,
                'klarna_order'         => $klarnaOrder,
                'response_code_object' => $responseCodeObject,
                )
            );

            Mage::dispatchEvent(
                'kco_push_notification_after', array(
                'order'                => $order,
                'klarna_order'         => $klarnaOrder,
                'response_code_object' => $responseCodeObject,
                )
            );

            // Update order references
            $api->updateMerchantReferences($checkoutId, $order->getIncrementId());

            // Acknowledge order
            if ($order->getState() != Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW && !$klarnaOrder->getIsAcknowledged()) {
                $api->acknowledgeOrder($checkoutId);
                $order->addStatusHistoryComment('Acknowledged request sent to Klarna');
                $klarnaOrder->setIsAcknowledged(1);
                $klarnaOrder->save();
            }

            // Cancel order in Klarna if cancelled on store
            if ($order->isCanceled()) {
                $this->_cancelFailedOrder($klarnaOrder->getKlarnaReservationId(), $order->getStore(), 'Order was already canceled in Magento');
            }

            $order->save();
        } catch (Klarna_Kco_Exception $e) {
            $responseCodeObject->setResponseCode(500);
            $cancelObject = new Varien_Object(
                array(
                'cancel_order' => true
                )
            );
            Mage::dispatchEvent(
                'kco_push_notification_order_not_found', array(
                'klarna_order_id'      => $checkoutId,
                'cancel_object'        => $cancelObject,
                'response_code_object' => $responseCodeObject,
                'controller_action'    => $this
                )
            );
            if ($cancelObject->getCancelOrder()) {
                $klarnaQuote = Mage::getModel('klarna_kco/klarnaquote')->loadByCheckoutId($checkoutId);
                if ($klarnaQuote->getId()) {
                    $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($klarnaQuote->getQuoteId());
                    if ($quote->getId() && $checkoutHelper->getDelayedPushNotification($quote->getStore())) {
                        $api = $helper->getApiInstance($quote->getStore());
                        $api->initKlarnaCheckout($checkoutId);
                        $reservationId = $api->getReservationId();
                        $this->_cancelFailedOrder($reservationId, $quote->getStore(), $e->getMessage());

                        // Only log if api supports Delayed Push Notification.
                        // Otherwise, the exception log will be full of useless logs due to race conditions.
                        Mage::logException($e);
                        Mage::log('Canceling order due to exception. checkout-id: ' . $checkoutId, Zend_Log::ALERT, 'klarna_errors.log');
                    }
                }
            }
        } catch (Exception $e) {
            $responseCodeObject->setResponseCode(500);
            Mage::dispatchEvent(
                'kco_push_notification_failed', array(
                'order'                => $order,
                'klarna_order_id'      => $checkoutId,
                'response_code_object' => $responseCodeObject,
                'controller_action'    => $this
                )
            );
            Mage::logException($e);
        }

        $this->getResponse()->setHttpResponseCode($responseCodeObject->getResponseCode());
    }

    /**
     * API call to validate a quote via callback from Klarna before the order is placed
     */
    public function validateAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $checkoutId = $this->getRequest()->getParam('id');

        try {
            $klarnaQuote = Mage::getModel('klarna_kco/klarnaquote')->loadByCheckoutId($checkoutId);
            if (!$klarnaQuote->getId() || $klarnaQuote->getIsChanged()) {
                Mage::log('Klarna order not found for checkout-id: ' . $checkoutId, Zend_Log::ALERT, 'klarna_errors.log');
                $this->_setValidateFailedResponse();
                return;
            }

            $quote = Mage::getModel('sales/quote')->load($klarnaQuote->getQuoteId());
            if (!$quote->getId() || !$quote->hasItems() || $quote->getHasError()) {
                Mage::log('Magento quote not found for checkout-id: ' . $checkoutId . ' expecting quote-id: ' . $klarnaQuote->getQuoteId(), Zend_Log::ALERT, 'klarna_errors.log');
                $this->_setValidateFailedResponse();
                return;
            }

            // Reserve Order ID
            $quote->reserveOrderId();
            $quote->save();

            $body = $this->getRequest()->getRawBody();

            try {
                $checkout = Mage::helper('core')->jsonDecode($body);
                $checkout = new Varien_Object($checkout);
            } catch (Exception $e) {
                Mage::log('Error decoding json for checkout-id: ' . $checkoutId, Zend_Log::ALERT, 'klarna_errors.log');
                $this->_sendBadRequestResponse();
                return;
            }

            // Set address is if it's not set
            if (($quote->isVirtual() && true !== $quote->getShippingAddress()->validate())
                || true !== $quote->getBillingAddress()->validate()
            ) {
                $this->getKco()->setQuote($quote);
                $this->_updateOrderAddresses($checkout);
            }

            $checkoutType = Mage::helper('klarna_kco/checkout')->getCheckoutType($quote->getStore());

            Mage::dispatchEvent(
                "kco_validate_before_order_place_type_{$checkoutType}", array(
                'quote'    => $quote,
                'checkout' => $checkout,
                'response' => $this->getResponse()
                )
            );

            Mage::dispatchEvent(
                'kco_validate_before_order_place', array(
                'quote'    => $quote,
                'checkout' => $checkout,
                'response' => $this->getResponse()
                )
            );

            try {
                $this->_validateOrderTotal($checkout, $quote);
            } catch (Klarna_Kco_Exception $e) {
                Mage::logException($e);
                $this->_setValidateFailedResponse();

                return;
            }

            $this->getResponse()->setHttpResponseCode(200);
        } catch (Klarna_Kco_Exception $e) {
            Mage::logException($e);
            // Do not modify header or log exception
        } catch (Klarna_Kco_Model_Api_Exception $e) {
            Mage::logException($e);
            $this->getResponse()->setHeader('HTTP/1.1', '503 Service Unavailable')->sendResponse();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->getResponse()->setHttpResponseCode(500);
        }
    }

    /**
     * Set the response that validation has failed
     *
     * @throws Zend_Controller_Response_Exception
     */
    protected function _setValidateFailedResponse()
    {
        $checkoutId = $this->getRequest()->getParam('id');
        $failUrl    = Mage::getUrl(
            'checkout/klarna/validateFailed', array(
            '_nosid'  => true,
            '_escape' => false,
            '_query'  => array('id' => $checkoutId),
            )
        );
        $this->getResponse()
            ->setHttpResponseCode(303)
            ->setHeader('Location', $failUrl);
    }

    /**
     * Order update from pending status
     */
    public function notificationAction()
    {
        $helper         = Mage::helper('klarna_kco');
        $checkoutHelper = Mage::helper('klarna_kco/checkout');

        if (!$this->getRequest()->isPost()) {
            return;
        }

        $checkoutId = $this->getRequest()->getParam('id');

        try {
            $klarnaOrder = Mage::getModel('klarna_kco/klarnaorder')->loadByCheckoutId($checkoutId);
            $order       = Mage::getModel('sales/order')->load($klarnaOrder->getOrderId());

            if (!$order->getId()) {
                throw new Klarna_Kco_Exception('Order not found');
            }

            $body = $this->getRequest()->getRawBody();

            try {
                $notification = Mage::helper('core')->jsonDecode($body);
                $notification = new Varien_Object($notification);
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_sendBadRequestResponse();

                return;
            }

            /** @var Mage_Sales_Model_Order_Payment $payment */
            $payment = $order->getPayment();

            if ($order->getState() == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
                switch ($notification->getEventType()) {
                    case Klarna_Kco_Model_Api_Kasper::ORDER_NOTIFICATION_FRAUD_REJECTED:
                        $payment->setNotificationResult(true);
                        $payment->registerPaymentReviewAction(Mage_Sales_Model_Order_Payment::REVIEW_ACTION_DENY, false);
                        break;
                    case Klarna_Kco_Model_Api_Kasper::ORDER_NOTIFICATION_FRAUD_ACCEPTED:
                        $payment->setNotificationResult(true);
                        $payment->registerPaymentReviewAction(Mage_Sales_Model_Order_Payment::REVIEW_ACTION_ACCEPT, false);
                        break;
                }

                $statusObject = new Varien_Object(
                    array(
                    'status' => $checkoutHelper->getProcessedOrderStatus($order->getStore())
                    )
                );

                Mage::dispatchEvent(
                    'kco_push_notification_before_set_state', array(
                    'order'         => $order,
                    'klarna_order'  => $klarnaOrder,
                    'status_object' => $statusObject
                    )
                );

                if (Mage_Sales_Model_Order::STATE_PROCESSING == $order->getState()) {
                    $order->addStatusHistoryComment(
                        $helper->__('Order processed by Klarna.'),
                        $statusObject->getStatus()
                    );
                }

                $order->save();
            } elseif (Klarna_Kco_Model_Api_Kasper::ORDER_NOTIFICATION_FRAUD_REJECTED == $notification->getEventType()
                && $order->getState() != Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW
            ) {
                $payment->setNotificationResult(false);
                $payment->setIsFraudDetected(true);
                $payment->registerPaymentReviewAction(Mage_Sales_Model_Order_Payment::REVIEW_ACTION_DENY, false);
                $order->save();
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}
